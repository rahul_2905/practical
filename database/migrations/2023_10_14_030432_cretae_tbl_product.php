<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_product', function (Blueprint $table) {
            $table->id();
            $table->integer('product_category_id')->unsigned();
            $table->string('name', 200);
            $table->string('slug', 200);
            $table->float('price', 8,2)->default(0);
            $table->text('description')->nullable();
            $table->string('image', 200)->nullable();
            $table->string('tag', 500)->comment('commaa sepreted values')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_product');
    }
};
