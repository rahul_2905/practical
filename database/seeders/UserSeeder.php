<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Database\Factories\PteMockFactory;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(array(
            'name' => 'Sample user1',
            'password' => bcrypt('123456789'),
            'email' => 'user1@gmail.com',
            'user_type' => 1 # user/customer
        ));
    }
}
