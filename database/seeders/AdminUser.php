<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Database\Factories\PteMockFactory;


class AdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(array(
            'name' => 'Super Admin',
            'password' => bcrypt('123456789'),
            'email' => 'admin@gmail.com',
            'user_type' => 0 #admin as 0 user
        ));
    }
}
