<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;



class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # get the category id from category table...
        $cat_array = [];
        $cat = new Category();
        $cate_id = $cat->getCategoryId();
        foreach($cate_id as $k=> $v) {
            $cat_array[] = $v->id; 
        }

        #  get the tage name
        $tag = ['black', 'blue', 'light', 'long', 'short', 'stunning'];
        shuffle($tag);
        shuffle($tag);
        $tagval = $tag[0].','.$tag[1];


        $faker = Faker::create();
        foreach (range(1, 200) as $index)  {
            DB::table('tbl_product')->insert([
                'product_category_id'=> array_rand($cat_array, 1),
                'name' => $faker->city,
                'slug' => $faker->unique()->slug,
                'price' => $faker->numberBetween($min = 500, $max = 8000),
                'description'=> $faker->paragraph($nb =8),
                'image' => $faker->image(public_path('images'),400,300, null, false),
                'tag'=> $tagval #implode(',',array_rand(['black', 'blue', 'light', 'long', 'short', 'stunning'],2))
            ]);
        }

    }
}