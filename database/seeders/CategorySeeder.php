<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use Database\Factories\PteMockFactory;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = Category::create(array(
            'name' => 'Clothes',
            'parent_id' => 0,
        ));

        $user = Category::create(array(
            'name' => 'Women Wear',
            'parent_id' => 1,
        ));
        $user = Category::create(array(
            'name' => "Men's Wear",
            'parent_id' => 1,
        ));
    }
}