<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

# login routes...
Route::any('/admin/login', 'Auth\LoginController@adminLogin');

/**
 * Admin Autheticate
 */
Route::group(['prefix'=>'admin','middleware' => ['auth:admin'] ], function(){
    Route::any('/dashboard', 'Admin\DashboardController@index');
    Route::any('/logout', 'Auth\LoginController@adminLogout');
});


/**
 * Admin Autheticate
 */
Route::group(['prefix'=>'product','middleware' => ['auth:web'] ], function(){
    Route::any('/', 'User\ProductController@index');
});
