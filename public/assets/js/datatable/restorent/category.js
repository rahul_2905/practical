app.category = {
    events: {
        switch: function () {
            $(document).on('click', ".change_status", function () {
                
                var id = $(this).attr('id');
                var status = $(this).attr('rel');
                var url = app.config.SITE_PATH + $(this).attr('formaction') +'/change-status';
                app.changeStatus(id, url, status);
            });
        },
        delete: function () {
            $(document).on('click', ".deleteRecord", function () {
                var dpath = $(this).attr('formaction');
                var result = confirm("Are you sure you want to delete this record?");
                if (result) {
                    var id = $(this).attr('rel');
                    var url = app.config.SITE_PATH + dpath +'/delete';
                    app.deleteRecord(id, url);
                }
            });
        },
        init: function () {
            app.category.events.switch();
            app.category.events.delete();
            app.dataTable.search();
            app.dataTable.reset();
        },
    },

    init: function () {
        app.category.events.init();
        app.dataTable.custom({"url":'restorent/category/datatable'});
        app.dataTable.eventFire();
    }
}