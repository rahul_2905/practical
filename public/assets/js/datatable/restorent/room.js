app.room = {
    events: {
        switch: function () {
            $(document).on('click', ".change_status", function () {
                
                var id = $(this).attr('id');
                var status = $(this).attr('rel');
                var url = app.config.SITE_PATH + $(this).attr('formaction') +'/change-status';
                app.changeStatus(id, url, status);
            });
        },
        delete: function () {
            $(document).on('click', ".deleteRecord", function () {
                var dpath = $(this).attr('formaction');
                var result = confirm("Are you sure you want to delete this record?");
                if (result) {
                    var id = $(this).attr('rel');
                    var url = app.config.SITE_PATH + dpath +'/delete';
                    app.deleteRecord(id, url);
                }
            });
        },
        init: function () {
            app.room.events.switch();
            app.room.events.delete();
            app.dataTable.search();
            app.dataTable.reset();
        },
    },

    init: function () {
        app.room.events.init();
        app.dataTable.custom({"url":'restorent/room/datatable'});
        app.dataTable.eventFire();
    }
}