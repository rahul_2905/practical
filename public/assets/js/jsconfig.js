/**
 * @file
 * A JavaScript file for Application configuration
 *
 */


// Define App and datatable variable

var app = app || {};
var dataTable, searchBtn;

app.config = {
    SITE_PATH: baseUrl,
    init: function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
};

// Log data in console
function l(data) {
    console.log(data);
}

// Check string is valid json or not
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

// generate random password
function generate_password(){
    var randomstring = Math.random().toString(36).slice(-8);
    $('#password').val(randomstring);
}


// Show loading mask
app.showLoader = function (id) { 
    $((id) ? id : 'body').append("<div class='loader'></div>"); 
}

// Hide loading mask
app.hideLoader = function (id) {
    $('.loader').css('display', 'none');
}

// Remove loading mask
app.removeLoader = function () {
    $('.loader').remove();
}

//Check for mobile device
var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPod/i);
    },
    ipad: function () {
        return navigator.userAgent.match(/iPad/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    BB: function () {
        return navigator.userAgent.match(/BB10/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() || isMobile.BB());
    }
};

// Check all check box
app.checkAll = function () {
    $('#checkAll').on('click', function () {
        $('.checkAllChild').prop('checked', $(this).is(":checked"));
    });

    $('.checkAllChild').on('click', function () {
        if ($(this).is(":checked")) {
            if ($('.checkAllChild').length === $('.checkAllChild:checked').length) {
                $('#checkAll').prop('checked', $(this).is(":checked"));
            }
        } else {
            $('#checkAll').prop('checked', $(this).is(":checked"));
        }
    });
}

// change status

app.changeStatus = function (id,url,status) {

    $.ajax({
        type: "POST",
        url: url,
        data: { id : id,status:status,_token: csrf_token}
    }).done(function(data){
        location.reload();
    });
}


// change status

app.changeLeadPriority = function (id,url,lead_priority) {

    $.ajax({
        type: "POST",
        url: url,
        data: { id : id,lead_priority:lead_priority,_token: csrf_token}
    }).done(function(data){
       location.reload();
    });
}

// Delete Record

app.deleteRecord = function (id,url) {
    $.ajax({
        type: "POST",
        url: url,
        data: { id : id,_token: csrf_token}
    }).done(function(data){
        location.reload();
    });
}

// change lead status

app.changeLeadstatus = function (id,url,lead_status_id) {

    $.ajax({
        type: "POST",
        url: url,
        data: { id : id,lead_status_id:lead_status_id,_token: csrf_token}
    }).done(function(data){
        location.reload();
    });
}

// datepicker
app.datepicker = function (param) {
    $("#"+param).datetimepicker({
        formatDate: 'd.m.Y',
        format:'d-M-Y',
        timepicker:false,
        closeOnDateSelect:true
    });
}

app.validate = {
    init:function (params) {
        if(typeof params != undefined) {
            $("#app_form").validate(params);
        }else{
            $("#app_form").validate();
        }
    }
}

// Hide Flash and validation message
$(function() {
    setTimeout(function(){
        $(".card-alert").slideUp(500).html("");
    },5000);

    $(".close").click(function(){
        $(".card-alert").hide();
    });

});
app.changeState = function (id,url,state_id) {
    $.ajax({
        type: "POST",
        url: url,
        data: { country_id : id,state_id : state_id,  _token: csrf_token}
    }).done(function(data){
        $("#state_id").html(data.stateList);
    });
}

app.selectedCountryState = function () {
    var name = 'country';
    var id = $('select[name="' + name + '"]').val();
    var state_id = $('#selected_state').val();
    var url = app.config.SITE_PATH+'getstate';
    app.changeState(id, url,state_id);
}

$('#country_id').change(function () {
    app.selectedCountryState();
});

$('.numeric').keypress(function(event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});