@extends('admin.layout.app')
@section('pageTitle')
    {{__('app.dashboard_title',["app_name" => __('app.app_name')])}}
@endsection
@push('externalCssLoad')

@endpush
@push('internalCssLoad')
@endpush
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="row mt-3">
                <div class="card bg-white col-md-12">
                    <div class="card-header">Report OF Product visited</div>
                </div>
                <div class="card-body">
                    <div id="chart"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('externalJsLoad')
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
@endpush
@push('internalJsLoad')

<script>
 
 var options = {
          series: [{
            name: "Desktops",
            data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
        }],
          chart: {
          height: 350,
          type: 'line',
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'straight'
        },
        title: {
          text: 'Product Trends by Month',
          align: 'left'
        },
        grid: {
          row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
          },
        },
        xaxis: {
          categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
</script>
@endpush