@php $version = time(); @endphp
<html lang="en">
<head>
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle')</title>

    <!--- theme css- -->
    <link href="{{ url('assets/css/theme/bootstrap.min.css')}}" rel="stylesheet">    
    <link href="{{ url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ url('assets/css/theme/animate.css')}}" rel="stylesheet">
    <link href="{{ url('assets/css/theme/style.css')}}" rel="stylesheet">
    <link href="{{ url('assets/css/theme/custom.css')}}" rel="stylesheet">


    @stack('externalCss')
    @stack('internalCss')
</head>
<!-- body start -->
<body class="pace-done">
    <div id="wrapper">
        @include('admin.layout.sidebar')
        <div id="page-wrapper" class="gray-bg">
            @include('admin.layout.header')
            @yield('content')
            @include('admin.layout.footer')
        </div>        
    </div><!-- END wrapper -->
        
<!-- Mainly scripts -->
<script src="{{ url('assets/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{ url('assets/js/popper.min.js')}}"></script>             
<script src="{{ url('assets/js/bootstrap.js')}}"></script>
<script src="{{ url('assets/plugins/metisMenu/jquery.metisMenu.js')}}"></script>       
<script src="{{ url('assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- alertify scripts -->
<script src="{{ url('assets/plugins/toastr/toastr.min.js')}}"></script>


<script type="text/javascript">
    $('#side-menu').metisMenu({toggle: false });
    var baseUrl = "{{ url('/') }}/";
    var csrf_token = "<?php echo csrf_token(); ?>";
</script>


<!--- app.js -->
<!-- <script src="{{ url('js/app.js') }}"></script> -->

@stack('externalJs')
@stack('internalJs')
</body>
</html>