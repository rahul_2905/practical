<div class="row border-bottom">
   <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
         <a class="navbar-minimalize minimalize-styl-2 btn btn-primary p-2" href="#"><i class="fa fa-bars"></i> </a>
         <form role="search" class="navbar-form-custom" action="http://webapplayers.com/inspinia_admin-v2.9.4/search_results.html">
            <div class="form-group">
               <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
            </div>
         </form>
      </div>
      <ul class="nav navbar-top-links navbar-right">
         <li>
            <span class="m-r-sm text-muted welcome-message">Welcome to Admin</span>
         </li>
         <li>
            <a href="{{ url('admin/logout')}}">
                <i class="fa fa-sign-out"></i> Log out
            </a>
         </li>
      </ul>
   </nav>
</div>