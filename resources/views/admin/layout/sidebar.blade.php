<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse ">
        <ul class="nav metismenu" id="side-menu">
            <li class="active">
                <a href="{{ url('admin/dashboard')}}">
                    <i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span>
                </a>
            </li>
            </ul>
        </li>
    </div>
</nav>