<?php

namespace App\Http\Traits;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Client\Request;

trait UsersAuthenticateTrait {

    /**
     * @param Request $request
     * @return Redirect
     * @desc Check admin login credentials valid or not
     */
    public function checkAdminLogin($request)
    {
        if (Auth::guard('admin')->attempt(['email' =>$request['email'], 'password' => $request['password'], 'user_type'=>0, 'status'=>1])) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param Request $request
     * @return Redirect
     * @desc Check for retail student login..
     * 
     * user_type 1 : customer as user...
     */
    public function checkUserLogin($request)
    {
        if (Auth::guard('student')->attempt(['email' =>$request['email'], 'password' => $request['password'], 'user_type'=>1, 'status'=>1])) {
            return true;
        }else{
            return false;
        }
    }
}