<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Traits\UsersAuthenticateTrait;
use Illuminate\Support\Facades\Session;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use UsersAuthenticateTrait;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function adminLogin(Request $request) {
        if($request->email != null) {
            $validated = $request->validate([
                'email' => 'required|email',
                'password' => 'required',
            ]);

            $data = $this->checkAdminLogin($request->all());
            if($data) {
                return redirect('admin/dashboard');
            } else {
                Session::flash('message', "Please enter valid credential.");
                return redirect('/admin/login')->withInput(['email']);
            }
        }
        return view('auth.admin.login');
    }

    /**
     * 
     * Logout the admin
     */
    public function adminLogout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }
}
