<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class DashboardController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $data = [];
        return view('admin.dashboard', $data);
    }
}
