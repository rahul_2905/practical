<?php

namespace App\Http\Controllers\User;;
use Auth;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

     protected $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [];
        $data['productData'] = $this->product->getCollection();
        return view('product.list', $data);
    }
}
