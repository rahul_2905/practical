<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class Category extends Model
{

    protected $table = 'tbl_category';
    protected $primaryKey = 'id';


    /**
     * Get all Category getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return Category::get();
    }

    public function getCategoryByField($id, $field_name)
    {
        return Category::where($field_name, $id)->first();
    }

    public function getCategoryId()
    {
        return Category::select('id')->get();
    }
}
