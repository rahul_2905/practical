<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;


class Product extends Model
{

    protected $table = 'tbl_product';
    protected $primaryKey = 'id';


    /**
     * Get all Product getCollection
     *
     * @return mixed
     */
    public function getCollection()
    {
        return Product::paginate(10);
    }

    public function getProductByField($id, $field_name)
    {
        return Product::where($field_name, $id)->first();
    }

    public function getProductId()
    {
        return Product::select('id')->get();
    }
}
